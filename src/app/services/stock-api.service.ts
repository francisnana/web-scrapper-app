import {Injectable} from '@angular/core';
import {Observable, throwError, timer} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, map, shareReplay, switchMap} from 'rxjs/operators';
import {IStock} from '../IStock';
import {WebStorageService} from './web-storage.service';

@Injectable({
  providedIn: 'root'
})
export class StockApiService {
  private API_URL = 'https://webscrapperassessment.herokuapp.com/stock';
  private STOCK_KEY = 'stock_key';
  private REFRESH_INTERVALL =  5 * 60 * 1000;

  constructor(private http: HttpClient, private storage: WebStorageService) {
  }

  getStockIndices(): Observable<IStock[]> {
    const timer$ = timer(0, this.REFRESH_INTERVALL);
    return timer$.pipe(
      switchMap(() => {
        return this.fetchStockIndices();
      }),
      shareReplay(1)
    );
  }

  private fetchStockIndices(): Observable<IStock[]> {
    return this.http.get(this.API_URL).pipe(
      map((data) => {
        this.storage.set(this.STOCK_KEY, data);
        return data as IStock[];
      }),
      catchError((error) => {
        const stock = this.storage.get(this.STOCK_KEY);
        if (!stock) {
          return throwError(error);
        } else {
          return throwError({data: stock, fetchFailed: true});
        }
      })
    );
  }


}
