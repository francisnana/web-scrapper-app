import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WebStorageService {

  constructor() {
  }

  set(key: string, value: any) {
    return localStorage.setItem(key, JSON.stringify(value));

  }

  get(key): any {
    const data = JSON.parse(localStorage.getItem(key));
    if (!data) {
      return null;
    } else {
      return data;
    }

  }
}
