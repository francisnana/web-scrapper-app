import {Component, OnInit} from '@angular/core';
import {StockApiService} from '../services/stock-api.service';
import {IStock} from '../IStock';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  error: boolean;
  loading: boolean;
  indicesDataSource: IStock[];

  constructor(private stockApi: StockApiService) {
  }

  ngOnInit() {
    this.getStocks();
  }

  getStocks() {
    this.loading = true;
    this.stockApi.getStockIndices().subscribe((indices) => {
      this.loading = false;
      this.error = false;
      this.indicesDataSource = indices;
    }, (error) => {
      this.loading = false;
      this.error = true;
      if (error.data) {
        this.indicesDataSource = error.data;
      }
      setTimeout(() => {
        this.getStocks();
      }, 10000);

    });
  }


}
