export interface IStock {
  name: string;
  rp: string;
  move: string;
  percentageMove: string;
}

